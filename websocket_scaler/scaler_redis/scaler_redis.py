from typing import List
import redis.asyncio as redis
from websocket_scaler.core.contract import AbstractScaler, AbstractWSClient
from websocket_scaler.core.message import (
    marshal_message_to_all,
    marshal_message_to_room,
    unmarshal_message_to_all,
    marshal_message_to_mulitple_user,
    unmarshal_message_to_multiple_user,
    marshal_message_to_single_user,
    unmarshal_message_to_room,
    unmarshal_message_to_single_user,
)


class ScalerRedis(AbstractScaler):
    def __init__(self, redis_url: str, channel: str) -> None:
        self.redis_url = redis_url
        self.redis_conn: redis.Redis = None
        self.channel = channel
        self.ws: AbstractWSClient = None
        self.STOP_MESSAGE = "STOP"
        super().__init__()

    async def send_to_single_user(self, socket_id: str, payload: str):
        message = marshal_message_to_single_user(socket_id=socket_id, payload=payload)
        await self.redis_conn.publish(channel=self.channel, message=message)

    async def send_to_multiple_user(self, socket_ids: List[str], payload: str):
        message = marshal_message_to_mulitple_user(
            socket_ids=socket_ids, payload=payload
        )
        await self.redis_conn.publish(channel=self.channel, message=message)

    async def send_to_all(self, payload: str):
        message = marshal_message_to_all(payload=payload)
        await self.redis_conn.publish(channel=self.channel, message=message)

    async def send_to_room(self, room: str, payload: str):
        message = marshal_message_to_room(room=room, payload=payload)
        await self.redis_conn.publish(channel=self.channel, message=message)

    async def subscribe(self, ws: AbstractWSClient):
        self.ws = ws
        self.redis_conn: redis.Redis = await redis.from_url(f"{self.redis_url}")
        pubsub = self.redis_conn.pubsub()
        await pubsub.subscribe(self.channel)
        while True:
            message = await pubsub.get_message(ignore_subscribe_messages=True)
            if message is not None:
                message = message.get("data", b"").decode()

                # Unsubscribe
                if message == self.STOP_MESSAGE:
                    break

                # Parse Message
                try:
                    data = unmarshal_message_to_single_user(message=message)
                    await ws.send_to_single_user(
                        socket_id=data["socket_id"], payload=data["payload"]
                    )
                except Exception:
                    pass

                try:
                    data = unmarshal_message_to_multiple_user(message=message)
                    await ws.send_to_multiple_user(
                        socket_ids=data["socket_ids"], payload=data["payload"]
                    )
                except Exception:
                    pass

                try:
                    data = unmarshal_message_to_all(message=message)
                    await ws.send_to_all(payload=data["payload"])
                except Exception:
                    pass

                try:
                    data = unmarshal_message_to_room(message=message)
                    await ws.send_to_room(room=data['room'], payload=data["payload"])
                except Exception:
                    pass

    async def unsubscribe(self):
        await self.redis_conn.publish(self.channel, self.STOP_MESSAGE)
