from typing import Any, List
from fastapi import WebSocket, WebSocketDisconnect
from websocket_scaler.core.contract import (
    AbstractWSClient,
)
from websocket_scaler.core.event import BaseEvent
from websocket_scaler.core.utils import generate_socket_id


class WSClientFastApi(AbstractWSClient):
    def __init__(self) -> None:
        self.conns: dict[str, WebSocket] = {}
        self.rooms: dict[str, list[str]] = {}
        super().__init__()

    async def send_to_single_user(self, socket_id: str, payload: str):
        conn = self.conns.get(socket_id)
        if conn is not None:
            await conn.send_text(payload)

    async def send_to_multiple_user(self, socket_ids: List[str], payload: str):
        for socket_id in socket_ids:
            conn = self.conns.get(socket_id)
            if conn is not None:
                conn.state
                await conn.send_text(payload)

    async def send_to_all(self, payload: str):
        for _, value in self.conns.items():
            await value.send_text(payload)

    async def send_to_room(self, room: str, payload: str):
        get_room = self.rooms.get(room, [])
        for socket_id in get_room:
            conn = self.conns.get(socket_id, None)
            if conn is not None:
                await conn.send_text(payload)

    async def add_user_to_room(self, socket_id: str, room: str):
        found_room = self.rooms.get(room, None)
        if found_room is None:
            self.rooms[room] = [socket_id]
        else:
            is_found = [x for x in self.rooms[room] if x == socket_id]
            if len(is_found) == 0:
                self.rooms[room].append(socket_id)

    async def remove_user_from_room(self, socket_id: str, room: str):
        found_room = self.rooms.get(room, None)
        if found_room is None:
            return

        if len(found_room) == 0:
            return

        self.rooms[room] = [x for x in self.rooms[room] if x != socket_id]

    async def fastapi_websocket_route(
        self, event: BaseEvent, websocket: WebSocket, extra: Any
    ):
        socket_id = generate_socket_id()
        self.conns[socket_id] = websocket
        await event.set_socket_id(socket_id=socket_id)
        await event.on_connect(extra=extra)
        try:
            while True:
                data = await websocket.receive_text()
                await event.on_message(payload=data)
        except WebSocketDisconnect:
            del self.conns[socket_id]
            for room_name, _ in self.rooms.items():
                await self.remove_user_from_room(socket_id=socket_id, room=room_name)
            await event.on_disconnect()
