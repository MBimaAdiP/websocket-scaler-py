import random
import string


def generate_socket_id() -> str:
    letters = string.ascii_lowercase + string.ascii_uppercase + string.digits
    result_str = "".join(random.choice(letters) for i in range(25))
    return result_str
