from typing import Any, Optional
from websocket_scaler.core.contract import (
    AbstractScaler,
    AbstractWSClient,
    AbstractEvent,
)


class BaseEvent(AbstractEvent):
    def __init__(
        self,
        scaler: AbstractScaler,
        ws_client: AbstractWSClient,
        socket_id: Optional[str] = None,
    ):
        self.socket_id = socket_id
        self.scaler = scaler
        self.ws_client = ws_client

    async def set_socket_id(self, socket_id):
        self.socket_id = socket_id

    async def send(self, payload: str):
        await self.ws_client.send_to_single_user(
            socket_id=self.socket_id, payload=payload
        )

    async def send_to_room(self, room: str, payload: str):
        await self.scaler.send_to_room(room=room, payload=payload)

    async def send_to_all(self, payload: str):
        await self.scaler.send_to_all(payload=payload)

    async def join_room(self, room: str):
        await self.ws_client.add_user_to_room(room=room, socket_id=self.socket_id)

    async def leave_room(self, room: str):
        await self.ws_client.remove_user_from_room(room=room, socket_id=self.socket_id)

    async def on_connect(self, extra: Any):
        pass

    async def on_message(self, payload: str):
        pass

    async def on_disconnect(self):
        pass
