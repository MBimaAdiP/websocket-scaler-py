import abc
from typing import Any, Optional


class AbstractScaler(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    async def send_to_single_user(self, socket_id: str, payload: str):
        pass

    @abc.abstractmethod
    async def send_to_multiple_user(self, socket_ids: list[str], payload: str):
        pass

    @abc.abstractmethod
    async def send_to_all(self, payload: str):
        pass

    @abc.abstractmethod
    async def send_to_room(self, room: str, payload: str):
        pass


class AbstractWSClient(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    async def send_to_single_user(self, socket_id: str, payload: str):
        pass

    @abc.abstractmethod
    async def send_to_multiple_user(self, socket_ids: list[str], payload: str):
        pass

    @abc.abstractmethod
    async def send_to_all(self, payload: str):
        pass

    @abc.abstractmethod
    async def send_to_room(self, room: str, payload: str):
        pass

    @abc.abstractmethod
    async def add_user_to_room(self, room:str, socket_id: str):
        pass

    @abc.abstractmethod
    async def remove_user_from_room(self, room:str, socket_id: str):
        pass


class AbstractEvent(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    async def on_connect(
        self, socket_id: str, extra: Optional[Any] = None
    ):
        pass

    @abc.abstractmethod
    async def on_message(self, socket_id: str, payload: str):
        pass

    @abc.abstractmethod
    async def on_disconnect(self, socket_id: str):
        pass
