import json
from typing import TypedDict


class MessageToSingleUserTypedDict(TypedDict):
    message_to_single_user: bool
    socket_id: str
    payload: str


class MessageToMultipleUserTypedDict(TypedDict):
    message_to_multiple_user: bool
    socket_ids: list[str]
    payload: str


class MessageToAllTypedDict(TypedDict):
    message_to_all: bool
    payload: str


class MessageToRoomTypedDict(TypedDict):
    message_to_room: bool
    room: str
    payload: str


def marshal_message_to_single_user(socket_id: str, payload: str) -> str:
    return json.dumps(
        {"message_to_single_user": True, "socket_id": socket_id, "payload": payload}
    )


def marshal_message_to_mulitple_user(socket_ids: list[str], payload: str) -> str:
    return json.dumps(
        {"message_to_multiple_user": True, "socket_ids": socket_ids, "payload": payload}
    )


def marshal_message_to_all(payload: str) -> str:
    return json.dumps({"message_to_all": True, "payload": payload})


def marshal_message_to_room(room: str, payload: str) -> str:
    return json.dumps({"message_to_room": True, "room": room, "payload": payload})


def unmarshal_message_to_single_user(
    message: str,
) -> MessageToSingleUserTypedDict:
    dictionary: dict = json.loads(message)

    validation = dictionary.get("message_to_single_user", None)
    if validation is None:
        raise Exception("invalid message key message_to_single_user not found")

    validation = dictionary.get("socket_id", None)
    if validation is None:
        raise Exception("invalid message key socket_id not found")

    validation = dictionary.get("payload", None)
    if validation is None:
        raise Exception("invalid message key payload not found")

    return dictionary


def unmarshal_message_to_multiple_user(
    message: str,
) -> MessageToMultipleUserTypedDict:
    dictionary: dict = json.loads(message)

    validation = dictionary.get("message_to_multiple_user", None)
    if validation is None:
        raise Exception("invalid message key message_to_multiple_user not found")

    validation = dictionary.get("socket_ids", None)
    if validation is None:
        raise Exception("invalid message key socket_ids not found")

    if type(validation) != list:
        raise Exception("invalid message key socket_ids, socket_ids should list of str")

    validation = dictionary.get("payload", None)
    if validation is None:
        raise Exception("invalid message key payload not found")

    return dictionary


def unmarshal_message_to_all(message: str) -> MessageToAllTypedDict:
    dictionary: dict = json.loads(message)

    validation = dictionary.get("message_to_all", None)
    if validation is None:
        raise Exception("invalid message key message_to_all not found")

    validation = dictionary.get("payload", None)
    if validation is None:
        raise Exception("invalid message key payload not found")

    return dictionary


def unmarshal_message_to_room(message: str) -> MessageToRoomTypedDict:
    dictionary: dict = json.loads(message)

    validation = dictionary.get("message_to_room", None)
    if validation is None:
        raise Exception("invalid message key message_to_room not found")

    validation = dictionary.get("room", None)
    if validation is None:
        raise Exception("invalid message key room not found")

    validation = dictionary.get("payload", None)
    if validation is None:
        raise Exception("invalid message key payload not found")

    return dictionary
