from typing import Literal, TypedDict
from websocket_scaler.core.contract import (
    AbstractWSClient,
)
from websocket_scaler.core.utils import generate_socket_id
from websocket_scaler.core.event import BaseEvent


class CommandLog(TypedDict):
    command: Literal[
        "send_to_single_user", "send_to_multiple_user", "send_to_all", "send_to_room"
    ]
    payload: str


class WSClientMock(AbstractWSClient):
    def __init__(self) -> None:
        self.logs: list[CommandLog] = []
        self.event: BaseEvent = None
        super().__init__()

    async def send_to_single_user(self, socket_id: str, payload: str):
        self.logs.append({"command": "send_to_single_user", "payload": payload})

    async def send_to_multiple_user(self, socket_ids: list[str], payload: str):
        self.logs.append({"command": "send_to_multiple_user", "payload": payload})

    async def send_to_all(self, payload: str):
        self.logs.append({"command": "send_to_all", "payload": payload})

    async def send_to_room(self, room: str, payload: str):
        self.logs.append({"command": "send_to_room", "payload": payload})

    async def add_user_to_room(self, room: str, socket_id: str):
        pass

    async def remove_user_from_room(self, room: str, socket_id: str):
        pass

    async def create_websocket_route(self, event: BaseEvent):
        self.event = event
        await self.event.set_socket_id(socket_id=generate_socket_id())

    async def call_on_connect(self):
        await self.event.on_connect()

    async def call_on_message(self, payload: str):
        await self.event.on_message(payload=payload)

    async def call_on_disconnect(self):
        await self.event.on_disconnect()
