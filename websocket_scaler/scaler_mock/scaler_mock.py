from typing import List, Literal, TypedDict
from websocket_scaler.core.contract import AbstractScaler, AbstractWSClient


class CommandLog(TypedDict):
    command: Literal[
        "send_to_single_user", "send_to_multiple_user", "send_to_all", "send_to_room"
    ]
    payload: str


class ScalerMock(AbstractScaler):
    def __init__(self):
        self.logs: list[CommandLog] = []
        self.ws: AbstractWSClient = None
        super().__init__()

    async def send_to_single_user(self, socket_id: str, payload: str):
        await self.ws.send_to_single_user(socket_id=socket_id, payload=payload)
        self.logs.append({"command": "send_to_single_user", "payload": payload})

    async def send_to_multiple_user(self, socket_ids: List[str], payload: str):
        await self.ws.send_to_multiple_user(socket_ids=socket_ids, payload=payload)
        self.logs.append({"command": "send_to_multiple_user", "payload": payload})

    async def send_to_all(self, payload: str):
        await self.ws.send_to_all(payload=payload)
        self.logs.append({"command": "send_to_all", "payload": payload})

    async def send_to_room(self, room: str, payload: str):
        await self.ws.send_to_room(payload=payload, room=room)
        self.logs.append({"command": "send_to_room", "payload": payload})

    async def subscribe(self, ws: AbstractWSClient):
        self.ws = ws
