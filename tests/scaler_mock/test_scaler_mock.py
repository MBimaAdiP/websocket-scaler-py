from unittest import TestCase
import pytest
from websocket_scaler.scaler_mock import ScalerMock
from websocket_scaler.wsclient_mock import WSClientMock


@pytest.mark.asyncio
async def test_mock_scaler():
    # Given
    scl = ScalerMock()
    ws_router = WSClientMock()
    await scl.subscribe(ws=ws_router)

    # When
    await scl.send_to_single_user("AAAAA", "Hello A")
    await scl.send_to_multiple_user(["AAAAA", "BBBBB"], "Hello A")
    await scl.send_to_all("Hello A")
    await scl.send_to_room(room="room", payload="Hello Room")

    # Expect
    TestCase().assertListEqual(
        ws_router.logs,
        [
            {"command": "send_to_single_user", "payload": "Hello A"},
            {"command": "send_to_multiple_user", "payload": "Hello A"},
            {"command": "send_to_all", "payload": "Hello A"},
            {"command": "send_to_room", "payload": "Hello Room"},
        ],
    )
