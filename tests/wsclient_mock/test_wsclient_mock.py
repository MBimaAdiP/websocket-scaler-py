import pytest
from websocket_scaler.wsclient_mock import WSClientMock
from websocket_scaler.scaler_mock import ScalerMock
from websocket_scaler.core.event import BaseEvent
from unittest import TestCase


@pytest.mark.asyncio
async def test_wsclient_mock():
    # Given
    ws_router = WSClientMock()

    # When
    await ws_router.send_to_single_user(socket_id="a", payload="Hello a")
    await ws_router.send_to_multiple_user(socket_ids=["a", "b"], payload="Hello a, b")
    await ws_router.send_to_all(payload="Hello World")

    # Expect
    TestCase().assertListEqual(
        ws_router.logs,
        [
            {"command": "send_to_single_user", "payload": "Hello a"},
            {"command": "send_to_multiple_user", "payload": "Hello a, b"},
            {"command": "send_to_all", "payload": "Hello World"},
        ],
    )


scaler = ScalerMock()


class Event(BaseEvent):
    async def on_connect(self):
        # await scaler.send_to_single_user(socket_id=socket_id, payload="Hello")
        await self.send(payload="Hello")

    async def on_message(self, payload: str):
        await self.send_to_all(f"got message {payload}")

    async def on_disconnect(self):
        await self.send_to_all("user disconnected")


ws_router = WSClientMock()


@pytest.mark.asyncio
async def test_wsclient_mock_create_route():
    # Given
    await scaler.subscribe(ws=ws_router)
    await ws_router.create_websocket_route(
        event=Event(scaler=scaler, ws_client=ws_router)
    )

    # When
    await ws_router.call_on_connect()
    await ws_router.call_on_message(payload="Hello everyone")
    await ws_router.call_on_disconnect()

    # Expect
    TestCase().assertListEqual(
        scaler.logs,
        [
            # {"command": "send_to_single_user", "payload": "Hello"},
            {
                "command": "send_to_all",
                "payload": "got message Hello everyone",
            },
            {
                "command": "send_to_all",
                "payload": "user disconnected",
            },
        ],
    )
