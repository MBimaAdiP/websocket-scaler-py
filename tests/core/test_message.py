from unittest import TestCase
from websocket_scaler.core.message import (
    marshal_message_to_all,
    marshal_message_to_single_user,
    unmarshal_message_to_all,
    unmarshal_message_to_single_user,
    marshal_message_to_mulitple_user,
    unmarshal_message_to_multiple_user,
    marshal_message_to_room,
    unmarshal_message_to_room,
)


def test_message_send_to_single_user():
    # When
    msg = marshal_message_to_single_user(socket_id="aaaa", payload="Hello World")
    dict_msg = unmarshal_message_to_single_user(msg)

    # Expect
    TestCase().assertDictEqual(
        dict_msg,
        {"message_to_single_user": True, "socket_id": "aaaa", "payload": "Hello World"},
    )


def test_message_send_to_multiple_user():
    # When
    msg = marshal_message_to_mulitple_user(
        socket_ids=["aaaa", "bbbb"], payload="Hello World"
    )
    dict_msg = unmarshal_message_to_multiple_user(msg)

    # Expect
    TestCase().assertDictEqual(
        dict_msg,
        {
            "message_to_multiple_user": True,
            "socket_ids": ["aaaa", "bbbb"],
            "payload": "Hello World",
        },
    )


def test_message_send_to_all():
    # When
    msg = marshal_message_to_all(payload="Hello World")
    dict_msg = unmarshal_message_to_all(msg)

    # Expect
    TestCase().assertDictEqual(
        dict_msg,
        {
            "message_to_all": True,
            "payload": "Hello World",
        },
    )


def test_message_send_to_room():
    # When
    msg = marshal_message_to_room(payload="Hello World", room="chat 1")
    dict_msg = unmarshal_message_to_room(msg)

    # Expect
    TestCase().assertDictEqual(
        dict_msg,
        {
            "message_to_room": True,
            "room": "chat 1",
            "payload": "Hello World",
        },
    )
