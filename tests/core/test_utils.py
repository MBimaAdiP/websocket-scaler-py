from websocket_scaler.core.utils import generate_socket_id
from unittest import TestCase


def test_generate_socket_id():
    # When
    socket_id = generate_socket_id()

    # Expect
    TestCase().assertIsInstance(socket_id, str)
