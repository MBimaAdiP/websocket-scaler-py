from typing import Optional
from unittest import TestCase
from fastapi import FastAPI, WebSocket
import pytest
from fastapi.testclient import TestClient

from websocket_scaler.core.event import BaseEvent
from websocket_scaler.scaler_mock.scaler_mock import ScalerMock
from websocket_scaler.wsclient_fastapi.wsclient_fastapi import WSClientFastApi

app = FastAPI()
ws_route = WSClientFastApi()
scl: Optional[ScalerMock] = ScalerMock()


class Event(BaseEvent):
    async def on_connect(self,extra):
        await self.send(payload=extra["path"])

    async def on_message(self,payload: str):
        pass

    async def on_disconnect(self):
        pass


@app.websocket("/ws/{id}")
async def websocket_endpoint(websocket: WebSocket, id: str):
    await websocket.accept()
    await ws_route.fastapi_websocket_route(
        event=Event(scaler=scl, ws_client=ws_route),
        websocket=websocket,
        extra={"path": id},
    )


@pytest.mark.asyncio
async def test_websocket_fastapi():
    # Given
    client = TestClient(app=app)
    await scl.subscribe(ws=ws_route)

    # Expect
    with client.websocket_connect("/ws/hai") as websocket1:
        data = websocket1.receive_text()
        TestCase().assertEqual(data, "hai")
