import asyncio
from typing import Optional
from unittest import TestCase
from fastapi import FastAPI, WebSocket
import pytest
from fastapi.testclient import TestClient

from websocket_scaler.core.event import BaseEvent
from websocket_scaler.scaler_mock.scaler_mock import ScalerMock
from websocket_scaler.wsclient_fastapi.wsclient_fastapi import WSClientFastApi

app = FastAPI()
ws_route = WSClientFastApi()
scl: Optional[ScalerMock] = ScalerMock()


class Event(BaseEvent):
    async def on_connect(self, extra):
        await self.send(payload="welcome")

    async def on_message(self, payload: str):
        if payload == "join":
            await self.join_room("chat")
        elif payload == "leave":
            await self.leave_room("chat")
        else:
            await self.send_to_room(room="chat", payload=payload)

    async def on_disconnect(self):
        pass


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    await websocket.accept()
    await ws_route.fastapi_websocket_route(
        event=Event(scaler=scl, ws_client=ws_route), websocket=websocket, extra=None
    )


@pytest.mark.asyncio
async def test_websocket_fastapi_room():
    # Given
    client = TestClient(app=app)
    await scl.subscribe(ws=ws_route)

    # Expect
    with client.websocket_connect("/ws") as websocket1:
        data = websocket1.receive_text()
        TestCase().assertEqual(data, "welcome")
        websocket1.send_text("join")
        await asyncio.sleep(1)
        TestCase().assertIsNotNone(ws_route.rooms.get("chat", None))
        with client.websocket_connect("/ws") as websocket2:
            websocket2.send_text("join")
            await asyncio.sleep(1)
            TestCase().assertIsNotNone(ws_route.rooms.get("chat", None))
            TestCase().assertEqual(len(ws_route.rooms["chat"]), 2)

            websocket2.send_text("leave")
            await asyncio.sleep(1)
            TestCase().assertEqual(len(ws_route.rooms["chat"]), 1)

            websocket2.send_text("hello")
            await asyncio.sleep(1)
            data = websocket1.receive_text()
            TestCase().assertEqual(data, "hello")
