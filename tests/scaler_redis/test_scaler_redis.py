import asyncio
from unittest import TestCase
import pytest
from websocket_scaler.scaler_redis import ScalerRedis
from websocket_scaler.wsclient_mock import WSClientMock


@pytest.mark.asyncio
async def test_scaler_redis():
    # Given
    scl = ScalerRedis(redis_url="redis://localhost", channel="ws_channel")
    ws_router = WSClientMock()
    asyncio.create_task(scl.subscribe(ws=ws_router))

    # When
    await asyncio.sleep(2)
    await scl.send_to_single_user("AAAAA", "Hello A")
    await scl.send_to_multiple_user(["AAAAA", "BBBBB"], "Hello A")
    await scl.send_to_all("Hello A")
    await scl.send_to_room(room="room", payload="Hello Room")
    await asyncio.sleep(2)

    # Expect
    TestCase().assertListEqual(
        ws_router.logs,
        [
            {"command": "send_to_single_user", "payload": "Hello A"},
            {"command": "send_to_multiple_user", "payload": "Hello A"},
            {"command": "send_to_all", "payload": "Hello A"},
            {"command": "send_to_room", "payload": "Hello Room"},
        ],
    )
    await scl.unsubscribe()
