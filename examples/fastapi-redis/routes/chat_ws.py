from fastapi import APIRouter, WebSocket
from websocket_scaler.core.event import BaseEvent
from redis_scaler import scl_redis
from ws_route import ws_route


router = APIRouter(prefix="/ws/chat", tags=["WS Chat"])


class Event(BaseEvent):
    async def on_connect(self, extra):
        self.username = extra
        await self.send(f"Welcome {extra}")
        await self.send_to_all(f"{extra} join the chat")
        await self.join_room(room="chat")

    async def on_message(self, payload: str):
        await self.send_to_room(
            room="chat",
            payload=payload,
        )

    async def on_disconnect(self):
        await self.send_to_all(f"{self.username} leave chat")


@router.websocket("/{username}")
async def new_chat(websocket: WebSocket, username: str):
    await websocket.accept()
    await ws_route.fastapi_websocket_route(
        event=Event(scaler=scl_redis, ws_client=ws_route),
        websocket=websocket,
        extra=username,
    )
