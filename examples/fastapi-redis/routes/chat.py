from fastapi import APIRouter
from fastapi.responses import JSONResponse
from pydantic import BaseModel
from redis_scaler import scl_redis


class MessageRequest(BaseModel):
    message: str


router = APIRouter(prefix="/chat", tags=["WS Chat"])


@router.post("/admin")
async def message_from_admin(request: MessageRequest):
    await scl_redis.send_to_all(payload=request.message)
    return JSONResponse(content={"message": "ok"}, status_code=200)
