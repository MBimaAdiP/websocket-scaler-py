import asyncio
from fastapi import FastAPI
from redis_scaler import scl_redis
from routes.chat_ws import router as chat_ws_router, ws_route
from routes.chat import router as chat_router

app = FastAPI()


@app.on_event("startup")
async def startup_event():
    asyncio.create_task(scl_redis.subscribe(ws=ws_route))


@app.on_event("shutdown")
async def shutdown_event():
    if scl_redis is not None:
        await scl_redis.unsubscribe()


app.include_router(chat_ws_router)
app.include_router(chat_router)
