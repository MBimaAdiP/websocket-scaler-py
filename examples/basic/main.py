import asyncio
from fastapi import FastAPI, WebSocket
from websocket_scaler.core.event import BaseEvent
from websocket_scaler.scaler_redis import ScalerRedis
from websocket_scaler.wsclient_fastapi import WSClientFastApi

app = FastAPI()
scl_redis = ScalerRedis(redis_url="redis://localhost", channel="ws_channel")
ws_route = WSClientFastApi()


class Event(BaseEvent):
    async def on_connect(self, extra):
        pass

    async def on_message(self, payload: str):
        await self.send_to_all(payload=payload)

    async def on_disconnect(self):
        pass


@app.on_event("startup")
async def startup_event():
    asyncio.create_task(scl_redis.subscribe(ws=ws_route))


@app.on_event("shutdown")
async def shutdown_event():
    if scl_redis is not None:
        await scl_redis.unsubscribe()


@app.websocket("/ws")
async def websocket_route(websocket: WebSocket):
    await websocket.accept()
    await ws_route.fastapi_websocket_route(
        event=Event(scaler=scl_redis, ws_client=ws_route),
        websocket=websocket,
        extra=None,
    )
